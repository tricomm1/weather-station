FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# ขั้นตอนที่ 2: ตั้งค่า Nginx ให้ให้บริการไฟล์ที่ build แล้ว
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html

# ถ้าคุณมีไฟล์ config เฉพาะสำหรับ Nginx ให้คัดลอกไฟล์นั้นมาที่นี่
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]







# # Stage 1: Build the Vue 3 app
# FROM node:latest as build-stage

# WORKDIR /app
# COPY package*.json ./
# RUN npm install -g npm@10.2.0
# RUN npm install
# COPY . .
# RUN npm run build

# # Stage 2: Serve the app with Nginx
# FROM nginx:alpine

# # Remove the default Nginx configuration
# RUN rm -rf /usr/share/nginx/html/*

# # Copy your custom Nginx configuration file
# COPY nginx.conf /etc/nginx/conf.d/default.conf

# # Copy the built app from the previous stage
# COPY --from=build-stage /app/dist /usr/share/nginx/html

# EXPOSE 80

# CMD ["nginx", "-g", "daemon off;"]
