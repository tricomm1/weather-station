@echo off
docker login registry.gitlab.com
set /p "tag=Tag : "
docker build -f ./Dockerfile.Nginx -t registry.gitlab.com/tricomm1/weather-station:%tag% .
docker push registry.gitlab.com/tricomm1/weather-station:%tag%
pause