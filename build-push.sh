# REGISTRY=registry.gitlab.com/unicon-dev-team/unicon-big-data/dpm.web
# REGISTRY=registry.gitlab.com/siripimol.sir/weather-station
REGISTRY=registry.gitlab.com/tricomm1/weather-station
read -p "Buid with tag: " TAG
echo "--------------------------------------------------"
echo "STEP 0 : LOGIN TO REGISTRY"
docker login registry.gitlab.com
echo "--------------------------------------------------"
echo "STEP 1 : BUILD IMAGE WITH INPUT TAG >> $TAG"
# docker build --platform linux/amd64 -f ./Dockerfile.Nginx -t $REGISTRY:$TAG .
docker buildx build --platform linux/amd64 -f ./Dockerfile.Nginx -t $REGISTRY:$TAG .
echo "--------------------------------------------------"
echo "STEP 2 : PUSH IMAGE"
docker push $REGISTRY:$TAG
echo "--------------------------------------------------"
read -p "FINISHED"