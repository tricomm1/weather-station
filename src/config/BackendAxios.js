import axios from 'axios'

const BackendAxios = axios.create({
  baseURL: 'https://thingsboard.tricommtha.com/api/',
  withCredentials: false,
  headers: {
    'Content-type': 'application/json',
    Accept: 'application/json'
  }
})



export default BackendAxios
